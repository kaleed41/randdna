#include <string>
#include <random>

using std::string;

string randDNA(int s, string b, int n)
{
	std::string dna = "";
	int baseLength = (b.length() - 1);
	std::mt19937 eng1(s);
	std::uniform_int_distribution<> uniDis(0, baseLength);
	

	for(int i = 0; i < n; i++)
	{ 
		dna = dna + b.at(uniDis(eng1));
	}
	
	return dna;
}
